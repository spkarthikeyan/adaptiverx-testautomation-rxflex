package rxflex_7_package

import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
//import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

import internal.GlobalVariable

import com.kms.katalon.core.logging.KeywordLogger
import com.kms.katalon.core.util.KeywordUtil
//import MobileBuiltInKeywords as Mobile
//import WSBuiltInKeywords as WS
//import WebUiBuiltInKeywords as WebUI

import org.openqa.selenium.WebElement
import org.openqa.selenium.WebDriver
import org.openqa.selenium.By

import com.kms.katalon.core.mobile.keyword.internal.MobileDriverFactory
import com.kms.katalon.core.webui.driver.DriverFactory
import com.kms.katalon.core.testobject.RequestObject
import com.kms.katalon.core.testobject.ResponseObject
import com.kms.katalon.core.testobject.ConditionType
import com.kms.katalon.core.testobject.TestObjectProperty
import com.kms.katalon.core.mobile.helper.MobileElementCommonHelper
import com.kms.katalon.core.util.KeywordUtil

import com.kms.katalon.core.webui.exception.WebElementNotFoundException

import com.kms.katalon.core.logging.KeywordLogger
import com.kms.katalon.core.util.KeywordUtil


public class RXFLEX_Setup_Formulary {

	@Keyword
	static def create_new_formulary() {
		try {
			/*Use: To login to RxFLEX 7.0 */
			(new rxflex_7_package.RXFLEX_Helper()).login_RxFlex(GlobalVariable.gl_base_url, GlobalVariable.gl_user_name, GlobalVariable.gl_password)

			/*Use: Navigate to Setup > Formulary tab*/
			WebUI.click(findTestObject('OR-RXFLEX-7/OR-Setup/OR-Formulary/OR-Page-Create-New-Formulary/setup-tab'))

			WebUI.click(findTestObject('OR-RXFLEX-7/OR-Setup/OR-Formulary/OR-Page-Create-New-Formulary/setup-formulary'))

			/*Use: Select 'Create New Formulary' */
			WebUI.waitForElementClickable(findTestObject('OR-RXFLEX-7/OR-Setup/OR-Formulary/OR-Page-Create-New-Formulary/btn-create_new_formulary'), 1)

			WebUI.click(findTestObject('OR-RXFLEX-7/OR-Setup/OR-Formulary/OR-Page-Create-New-Formulary/btn-create_new_formulary'))

			/*Use: Select Line of Business */
			WebUI.click(findTestObject('OR-RXFLEX-7/OR-Setup/OR-Formulary/OR-Page-Create-New-Formulary/line-of-business-workers-comp'))

			WebUI.click(findTestObject('OR-RXFLEX-7/OR-Setup/OR-Formulary/OR-Page-Create-New-Formulary/line-of-business-mail'))

			WebUI.click(findTestObject('OR-RXFLEX-7/OR-Setup/OR-Formulary/OR-Page-Create-New-Formulary/line-of-business-commercial'))

			/*Use: Select a Customer from CUSTOMER list */
			WebUI.click(findTestObject('OR-RXFLEX-7/OR-Setup/OR-Formulary/OR-Page-Create-New-Formulary/customer-dropdown-arrow'))

			WebUI.verifyElementVisible(findTestObject('OR-RXFLEX-7/OR-Setup/OR-Formulary/OR-Page-Create-New-Formulary/customer-dropdown-values'))

			WebUI.click(findTestObject('OR-RXFLEX-7/OR-Setup/OR-Formulary/OR-Page-Create-New-Formulary/select-customer-dropdown-values'))

			/*Use: Select Effective as today's date from EFFECTIVE DATE  */
			WebUI.click(findTestObject('OR-RXFLEX-7/OR-Setup/OR-Formulary/OR-Page-Create-New-Formulary/effective-date-arrow'))

			WebUI.verifyElementVisible(findTestObject('OR-RXFLEX-7/OR-Setup/OR-Formulary/OR-Page-Create-New-Formulary/effective-date-check-date-visibile-or-not'))

			WebUI.click(findTestObject('OR-RXFLEX-7/OR-Setup/OR-Formulary/OR-Page-Create-New-Formulary/effective-date-check-date-visibile-or-not'))

			WebUI.click(findTestObject('OR-RXFLEX-7/OR-Setup/OR-Formulary/OR-Page-Create-New-Formulary/calendar-year-for-formulary-arrow'))

			WebUI.verifyElementVisible(findTestObject('OR-RXFLEX-7/OR-Setup/OR-Formulary/OR-Page-Create-New-Formulary/calendar-year-for-formulary-values'))

			WebUI.click(findTestObject('OR-RXFLEX-7/OR-Setup/OR-Formulary/OR-Page-Create-New-Formulary/calendar-year-for-formulary-values'))

			/*Use: To enter a Formulary Name */
			WebUI.setText(findTestObject('OR-RXFLEX-7/OR-Setup/OR-Formulary/OR-Page-Create-New-Formulary/formulary-name'), 'DRG Formulary Test')

			/*Use: To enter a Formulary Code*/
			WebUI.setText(findTestObject('OR-RXFLEX-7/OR-Setup/OR-Formulary/OR-Page-Create-New-Formulary/custom-formulary-code'), 'DRG Formulary Code 001')

			/*Use: To select a THERAPY CLASS from the dropdown list */
			WebUI.click(findTestObject('OR-RXFLEX-7/OR-Setup/OR-Formulary/OR-Page-Create-New-Formulary/therapy-class-arrow'))

			WebUI.scrollToElement(findTestObject('OR-RXFLEX-7/OR-Setup/OR-Formulary/OR-Page-Create-New-Formulary/therapy-class-values'), 0)

			WebUI.click(findTestObject('OR-RXFLEX-7/OR-Setup/OR-Formulary/OR-Page-Create-New-Formulary/therapy-class-values'))

			/*Use: To select 'Yes' Tiering list */
			WebUI.scrollToElement(findTestObject('OR-RXFLEX-7/OR-Setup/OR-Formulary/OR-Page-Create-New-Formulary/tier-list-yes'), 0)

			WebUI.click(findTestObject('OR-RXFLEX-7/OR-Setup/OR-Formulary/OR-Page-Create-New-Formulary/tier-list-yes'))

			/*Use: To select  'Yes' for Preventative Tier  */


			/*Use: To select Number of Tiers for TIERING list*/


			/*Use: To select different Tier Name from TIERS list*/

			/*Use: To select 'Yes' for UM list */
			WebUI.scrollToElement(findTestObject('OR-RXFLEX-7/OR-Setup/OR-Formulary/OR-Page-Create-New-Formulary/um-list-yes'), 0)

			WebUI.click(findTestObject('OR-RXFLEX-7/OR-Setup/OR-Formulary/OR-Page-Create-New-Formulary/um-list-yes'))

			/*Use: To select Edit Types from EDIT TYPES list  */


			/*Use: To enter Ticket Description */


			/*Use: To select Create Ticket */


			/*Use: To verify Saving message */

			/*Use: To verify Formulary ID */

			/*Use: To select SUBMIT  */

			/*Use: To select 'Yes or No' for submit message */

			/*Use: To select Submit in the message box */

			/*Use: To verify Saving message */

			/*Use: To verify Ticket status as 'Reviewing' */

			/*Use: To select Complete Ticket */

			/*Use: To select 'Yes or No' for Complete Ticket message */

			/*Use: To verify Ticket Owner or assignment status of 'Complete Ticket' action */

			/*Use: To select Complete Ticket */

			/*Use: To verify Saving message of Complete Ticket action*/

			/*Use: To verify Ticket status as 'Completed' after 'Complete Ticket' action  */

			/*Use: To Close the application */


			/*Use:  */

		}
		catch (WebElementNotFoundException e)
		{
			KeywordUtil.markFailed(e)
		}
		catch (Exception e) {
			KeywordUtil.markFailed(e.getMessage())
		}
	}

	@Keyword
	static def login(){
		(new rxflex_7_package.RXFLEX_Helper()).login_RxFlex(GlobalVariable.gl_base_url, GlobalVariable.gl_user_name, GlobalVariable.gl_password)

	}

	@Keyword
	static def access_formulary_tab(){
		try{
			(new rxflex_7_package.RXFLEX_Helper()).clickElement(findTestObject('OR-RXFLEX-7/OR-Setup/OR-Formulary/OR-Page-Create-New-Formulary/setup-tab'))
			(new rxflex_7_package.RXFLEX_Helper()).clickElement(findTestObject('OR-RXFLEX-7/OR-Setup/OR-Formulary/OR-Page-Create-New-Formulary/setup-formulary'))
			//clickElement.
		}
		catch (WebElementNotFoundException e)
		{
			KeywordUtil.markFailed(e)
		}
		catch (Exception e) {
			KeywordUtil.markFailed(e.getMessage())
		}
	}

}
