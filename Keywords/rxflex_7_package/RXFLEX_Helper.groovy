package rxflex_7_package
import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testcase.TestCaseFactory
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testdata.TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords

import internal.GlobalVariable

import MobileBuiltInKeywords as Mobile
import WSBuiltInKeywords as WS
import WebUiBuiltInKeywords as WebUI

import org.openqa.selenium.WebElement
import org.openqa.selenium.WebDriver
import org.openqa.selenium.By

import com.kms.katalon.core.mobile.keyword.internal.MobileDriverFactory
import com.kms.katalon.core.webui.driver.DriverFactory

import com.kms.katalon.core.testobject.RequestObject
import com.kms.katalon.core.testobject.ResponseObject
import com.kms.katalon.core.testobject.ConditionType
import com.kms.katalon.core.testobject.TestObjectProperty

import com.kms.katalon.core.mobile.helper.MobileElementCommonHelper
import com.kms.katalon.core.util.KeywordUtil

import com.kms.katalon.core.webui.exception.WebElementNotFoundException

import com.kms.katalon.core.logging.KeywordLogger
import com.kms.katalon.core.util.KeywordUtil


public class RXFLEX_Helper {

	@Keyword
	static def login_RxFlex(String url, String user_name, String password) {

		WebUI.navigateToUrl(url)

		WebUI.setText(findTestObject('OR-RXFLEX-7/OR-Page-Login/user-name'), user_name)

		WebUI.setEncryptedText(findTestObject('OR-RXFLEX-7/OR-Page-Login/password'), password)

		clickElement(findTestObject('OR-RXFLEX-7/OR-Page-Login/login-button'))
		afterLoggingIn()
		//if (WebUI.verifyTextPresent('Welcome', false)) {
		//CustomKeywords.'customlog.customlogmsg.afterLoggingIn'()
		//}
	}

	@Keyword
	static def afterLoggingIn() {

		try {

			KeywordLogger log = new KeywordLogger()
			log.logInfo("Verifying 'Drag column heading to group.' text present or not...")
			if (WebUI.verifyTextPresent('Drag column heading to group.', false)) {
				//CustomKeywords.'customlog.customlogmsg.afterLoggingIn'()
				KeywordUtil.markPassed("Yes. 'Drag column heading to group.' text present...")
				log.logInfo("Yes. 'Drag column heading to group.' text present...")
				log.logInfo('Logged in successfully !!!')
				//log.logInfo("Verifying 'Drag column heading to group.' text present or not...")
				KeywordUtil.markPassed("Logged in successfully")
			}
			else {
				log.logInfo("Unable to find 'Drag column heading to group.' text...")
				log.logInfo('Login Unsuccessfull!!!')
				KeywordUtil.markFailed("Login Unsuccessful")
			}
		}
		catch (WebElementNotFoundException e) {
			KeywordUtil.markFailed("Element not found")
			KeywordUtil.markFailed(e)
		}
		catch (Exception e) {
			KeywordUtil.markFailed(e)
		}
		//log.logInfo("Verifying 'Drag column heading to group.' text present or not...")
		//log.logInfo('Login Successful!!!')
		//KeywordUtil.logInfo("Verifying Welcome text present or not...")
		//WebDriver webDriver = DriverFactory.getWebDriver()
		//webDriver.navigate().refresh()

	}

	/**
	 * Refresh browser
	 */
	@Keyword
	def refreshBrowser() {
		KeywordUtil.logInfo("Refreshing")
		WebDriver webDriver = DriverFactory.getWebDriver()
		webDriver.navigate().refresh()
		KeywordUtil.markPassed("Refresh successfully")
	}

	/**
	 * Click element
	 * @param to Katalon test object
	 */
	@Keyword
	static def clickElement(TestObject to) {
		try {
			WebElement element = WebUiBuiltInKeywords.findWebElement(to);
			KeywordUtil.logInfo("Clicking element")
			element.click()
			KeywordUtil.markPassed("Element has been clicked")
		} catch (WebElementNotFoundException e) {
			KeywordUtil.markFailed("Element not found")
		} catch (Exception e) {
			KeywordUtil.markFailed("Fail to click on element")
		}
	}

	/**
	 * Get all rows of HTML table
	 * @param table Katalon test object represent for HTML table
	 * @param outerTagName outer tag name of TR tag, usually is TBODY
	 * @return All rows inside HTML table
	 */
	@Keyword
	def List<WebElement> getHtmlTableRows(TestObject table, String outerTagName) {
		WebElement mailList = WebUiBuiltInKeywords.findWebElement(table)
		List<WebElement> selectedRows = mailList.findElements(By.xpath("./" + outerTagName + "/tr"))
		return selectedRows
	}
}