<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>therapy-class-values</name>
   <tag></tag>
   <elementGuidId>dcc6ec3a-e3db-48c5-b9a4-611520322b80</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//select[@name='newTcsAssociation']//option[@value='15'][contains(text(),'2016 ArgusTest 123')]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>contains</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//select[@name='newTcsAssociation']//option[@value='15'][contains(text(),'2016 ArgusTest 123')]</value>
   </webElementProperties>
</WebElementEntity>
