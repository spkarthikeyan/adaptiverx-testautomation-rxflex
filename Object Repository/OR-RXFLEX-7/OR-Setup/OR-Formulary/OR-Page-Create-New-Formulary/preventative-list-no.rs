<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>preventative-list-no</name>
   <tag></tag>
   <elementGuidId>cd9e9d1f-8f62-4939-9348-7721effb2ff3</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//div[@class='filter-toggles preventative-tier-toggle small']//div[@value='0'][contains(text(),'No')]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//div[@class='filter-toggles preventative-tier-toggle small']//div[@value='0'][contains(text(),'No')]</value>
   </webElementProperties>
</WebElementEntity>
