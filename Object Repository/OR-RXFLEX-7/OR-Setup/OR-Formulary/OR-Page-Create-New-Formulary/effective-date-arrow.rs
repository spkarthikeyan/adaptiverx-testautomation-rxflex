<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>effective-date-arrow</name>
   <tag></tag>
   <elementGuidId>9b24efcc-b630-49e2-aff9-3c9e04a3eea3</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//span[@class='k-icon k-i-calendar']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//span[@class='k-icon k-i-calendar']</value>
   </webElementProperties>
</WebElementEntity>
