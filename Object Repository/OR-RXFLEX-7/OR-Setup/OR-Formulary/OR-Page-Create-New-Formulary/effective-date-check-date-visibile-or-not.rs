<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>effective-date-check-date-visibile-or-not</name>
   <tag></tag>
   <elementGuidId>2adcecf2-af4b-42a7-8361-a62ead91edb7</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//a[@title='Tuesday, March 26, 2019']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>contains</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//a[@title='Tuesday, March 26, 2019']</value>
   </webElementProperties>
</WebElementEntity>
