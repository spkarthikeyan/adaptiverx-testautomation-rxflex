<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>select-customer-dropdown-values</name>
   <tag></tag>
   <elementGuidId>d4528a8c-3a32-4cd5-99e8-4ccd1b420387</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//li[contains(text(), 'BSC-Basic')]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>contains</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//li[contains(text(), 'BSC-Basic')]</value>
   </webElementProperties>
</WebElementEntity>
