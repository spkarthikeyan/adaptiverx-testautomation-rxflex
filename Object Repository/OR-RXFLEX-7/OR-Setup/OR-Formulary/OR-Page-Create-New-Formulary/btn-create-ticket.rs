<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>btn-create-ticket</name>
   <tag></tag>
   <elementGuidId>06c4b4c4-dab6-491b-ad32-054938eafcaa</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//div[@class='k-button a-window-footer-button a-window-footer-button-save action-button']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//div[@class='k-button a-window-footer-button a-window-footer-button-save action-button']</value>
   </webElementProperties>
</WebElementEntity>
