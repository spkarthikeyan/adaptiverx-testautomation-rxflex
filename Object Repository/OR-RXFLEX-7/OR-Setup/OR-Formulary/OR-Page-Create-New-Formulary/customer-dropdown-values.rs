<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>customer-dropdown-values</name>
   <tag></tag>
   <elementGuidId>a7aa0711-b291-4c93-bd0a-4a4cca10a4c8</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//li[contains(text(), 'BSC-Basic')]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>contains</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//li[contains(text(), 'BSC-Basic')]</value>
   </webElementProperties>
</WebElementEntity>
