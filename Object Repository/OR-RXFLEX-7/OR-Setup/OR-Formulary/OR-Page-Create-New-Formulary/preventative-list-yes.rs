<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>preventative-list-yes</name>
   <tag></tag>
   <elementGuidId>9310af6c-b7ff-48f9-8bf5-2528f4b956af</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//div[@class='filter-toggles preventative-tier-toggle small']//div[@value='1'][contains(text(),'Yes')]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//div[@class='filter-toggles preventative-tier-toggle small']//div[@value='1'][contains(text(),'Yes')]</value>
   </webElementProperties>
</WebElementEntity>
