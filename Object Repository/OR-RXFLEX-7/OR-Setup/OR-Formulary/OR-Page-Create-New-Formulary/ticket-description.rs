<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>ticket-description</name>
   <tag></tag>
   <elementGuidId>3c079cef-21e2-4a69-8269-b6bfa459ad03</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//textarea[@placeholder='&lt;ENTER TICKET DESCRIPTION HERE>']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//textarea[@placeholder='&lt;ENTER TICKET DESCRIPTION HERE>']</value>
   </webElementProperties>
</WebElementEntity>
