<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>custom-formulary-code</name>
   <tag></tag>
   <elementGuidId>3a679af1-8eff-4bf5-bbff-031ee6973f4c</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//tr[@class='newFormulary customFormularyCode']//input[@name='fcgCustomCode']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//tr[@class='newFormulary customFormularyCode']//input[@name='fcgCustomCode']</value>
   </webElementProperties>
</WebElementEntity>
