import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

WebUI.openBrowser('')

//WebUI.navigateToUrl(GlobalVariable.gl_base_url)

//WebUI.setText(findTestObject('RXFLEX-OR/OR-RXFLEX-LOGIN/Page_Welcome to RxFlex/input_v 70190321_username'), GlobalVariable.gl_user_name)

//WebUI.setEncryptedText(findTestObject('RXFLEX-OR/OR-RXFLEX-LOGIN/Page_Welcome to RxFlex/input_v 70190321_password'), GlobalVariable.gl_password)

//WebUI.click(findTestObject('RXFLEX-OR/OR-RXFLEX-LOGIN/Page_Welcome to RxFlex/input'))

CustomKeywords.'rxflex_7_package.RXFLEX_Helper.login_RXFLEX'(GlobalVariable.gl_base_url, GlobalVariable.gl_user_name, GlobalVariable.gl_password)
//CustomKeywords.'rxflex_7_package.RXFLEX_Helper.login_RXFLEX'(GlobalVariable.gl_base_url, User_Name, Password)

