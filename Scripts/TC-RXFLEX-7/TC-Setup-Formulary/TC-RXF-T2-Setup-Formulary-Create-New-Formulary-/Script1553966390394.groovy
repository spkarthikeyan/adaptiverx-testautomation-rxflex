import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

WebUI.openBrowser('')
/*
WebUI.navigateToUrl('http://rxflex-fdb-qa.adaptiverxtest.com')
WebUI.setText(findTestObject('OR-RXFLEX-7/OR-Page-Login/user-name'), 'adaptive')
WebUI.setEncryptedText(findTestObject('OR-RXFLEX-7/OR-Page-Login/password'), '6ImJICLuGIhvO+y79SkTkw==')
WebUI.click(findTestObject('OR-RXFLEX-7/OR-Page-Login/login-button'))
//CustomKeywords.'rxflex_7_package.RXFLEX_Setup_Formulary.create_new_formulary'()
not_run: CustomKeywords.'rxflex_7_package.RXFLEX_Setup_Formulary.login'()

not_run: CustomKeywords.'rxflex_7_package.RXFLEX_Setup_Formulary.access_formulary_tab'()

not_run: CustomKeywords.'rxflex_7_package.RXFLEX_Setup_Formulary.create_new_formulary'()
*/
'/*Access Login function defined in Keywords > rxflex_7_package > Helper */'
CustomKeywords.'rxflex_7_package.RXFLEX_Setup_Formulary.login'()

'/*Use: Navigate to Setup > Formulary tab*/'
WebUI.click(findTestObject('OR-RXFLEX-7/OR-Setup/OR-Formulary/OR-Page-Create-New-Formulary/setup-tab'))

WebUI.click(findTestObject('OR-RXFLEX-7/OR-Setup/OR-Formulary/OR-Page-Create-New-Formulary/setup-formulary'))

'/*Use: Select \'Create New Formulary\' */'
WebUI.waitForElementClickable(findTestObject('OR-RXFLEX-7/OR-Setup/OR-Formulary/OR-Page-Create-New-Formulary/btn-create_new_formulary'), 
    1)

WebUI.click(findTestObject('OR-RXFLEX-7/OR-Setup/OR-Formulary/OR-Page-Create-New-Formulary/btn-create_new_formulary'))

not_run: WebUI.click(findTestObject('OR-RXFLEX-7/OR-Setup/OR-Formulary/OR-Page-Create-New-Formulary/line-of-business-workers-comp'))

not_run: WebUI.click(findTestObject('OR-RXFLEX-7/OR-Setup/OR-Formulary/OR-Page-Create-New-Formulary/line-of-business-mail'))

'/*Use: Select Line of Business */'
WebUI.click(findTestObject('OR-RXFLEX-7/OR-Setup/OR-Formulary/OR-Page-Create-New-Formulary/line-of-business-commercial'))

'/*Use: Select a Customer from CUSTOMER list */'
WebUI.click(findTestObject('OR-RXFLEX-7/OR-Setup/OR-Formulary/OR-Page-Create-New-Formulary/customer-dropdown-arrow'))

WebUI.verifyElementVisible(findTestObject('OR-RXFLEX-7/OR-Setup/OR-Formulary/OR-Page-Create-New-Formulary/customer-dropdown-values'))

WebUI.click(findTestObject('OR-RXFLEX-7/OR-Setup/OR-Formulary/OR-Page-Create-New-Formulary/select-customer-dropdown-values'))

'/*Use: Select Effective as todays date from EFFECTIVE DATE  */'
WebUI.click(findTestObject('OR-RXFLEX-7/OR-Setup/OR-Formulary/OR-Page-Create-New-Formulary/effective-date-arrow'))

WebUI.verifyElementVisible(findTestObject('OR-RXFLEX-7/OR-Setup/OR-Formulary/OR-Page-Create-New-Formulary/effective-date-check-date-visibile-or-not'))

WebUI.click(findTestObject('OR-RXFLEX-7/OR-Setup/OR-Formulary/OR-Page-Create-New-Formulary/effective-date-check-date-visibile-or-not'))

WebUI.click(findTestObject('OR-RXFLEX-7/OR-Setup/OR-Formulary/OR-Page-Create-New-Formulary/calendar-year-for-formulary-arrow'))

WebUI.verifyElementVisible(findTestObject('OR-RXFLEX-7/OR-Setup/OR-Formulary/OR-Page-Create-New-Formulary/calendar-year-for-formulary-values'))

WebUI.click(findTestObject('OR-RXFLEX-7/OR-Setup/OR-Formulary/OR-Page-Create-New-Formulary/calendar-year-for-formulary-values'))

'/*Use: To enter a Formulary Name */'
WebUI.setText(findTestObject('OR-RXFLEX-7/OR-Setup/OR-Formulary/OR-Page-Create-New-Formulary/formulary-name'), 'DRG Formulary Test')

'/*Use: To enter a Formulary Code*/'
WebUI.setText(findTestObject('OR-RXFLEX-7/OR-Setup/OR-Formulary/OR-Page-Create-New-Formulary/custom-formulary-code'), 'DRG Formulary Code 001')

'/*Use: To select a THERAPY CLASS from the dropdown list */'
WebUI.click(findTestObject('OR-RXFLEX-7/OR-Setup/OR-Formulary/OR-Page-Create-New-Formulary/therapy-class-arrow'))

WebUI.scrollToElement(findTestObject('OR-RXFLEX-7/OR-Setup/OR-Formulary/OR-Page-Create-New-Formulary/therapy-class-values'), 
    0)

WebUI.click(findTestObject('OR-RXFLEX-7/OR-Setup/OR-Formulary/OR-Page-Create-New-Formulary/therapy-class-values'))

'/*Use: To select "Yes" Tiering list */'
WebUI.scrollToElement(findTestObject('OR-RXFLEX-7/OR-Setup/OR-Formulary/OR-Page-Create-New-Formulary/tier-list-yes'), 0)

WebUI.click(findTestObject('OR-RXFLEX-7/OR-Setup/OR-Formulary/OR-Page-Create-New-Formulary/tier-list-yes'))

'/*Use: To select  'Yes' for Preventative Tier  */'
WebUI.click(findTestObject('OR-RXFLEX-7/OR-Setup/OR-Formulary/OR-Page-Create-New-Formulary/preventative-list-yes'))

'/*Use: To select Number of Tiers for TIERING list*/'
WebUI.scrollToElement(findTestObject('OR-RXFLEX-7/OR-Setup/OR-Formulary/OR-Page-Create-New-Formulary/number-of-tiers-arrow'), 
    0)

WebUI.click(findTestObject('OR-RXFLEX-7/OR-Setup/OR-Formulary/OR-Page-Create-New-Formulary/number-of-tiers-arrow'))

WebUI.click(findTestObject('OR-RXFLEX-7/OR-Setup/OR-Formulary/OR-Page-Create-New-Formulary/number-of-tiers-list-values'))

'/*Use: To select different Tier Name from TIERS list*/'
WebUI.scrollToElement(findTestObject('OR-RXFLEX-7/OR-Setup/OR-Formulary/OR-Page-Create-New-Formulary/tiers-name-arrow'), 
    0)

not_run: WebUI.verifyElementVisible(findTestObject('OR-RXFLEX-7/OR-Setup/OR-Formulary/OR-Page-Create-New-Formulary/tiers-name1-select'))

not_run: WebUI.click(findTestObject('OR-RXFLEX-7/OR-Setup/OR-Formulary/OR-Page-Create-New-Formulary/tiers-name1-select'))

not_run: WebUI.setText(findTestObject('OR-RXFLEX-7/OR-Setup/OR-Formulary/OR-Page-Create-New-Formulary/tiers-name1-select'), 
    'GENERICS')

WebUI.setText(findTestObject('OR-RXFLEX-7/OR-Setup/OR-Formulary/OR-Page-Create-New-Formulary/tiers-name1-set-value'), 'GENERICS')

WebUI.setText(findTestObject('OR-RXFLEX-7/OR-Setup/OR-Formulary/OR-Page-Create-New-Formulary/tiers-name2-set-value'), 'NON-PREFERRED GENERICS')

'/*To Close the tier name dropdown we need to click on other element'
WebUI.click(findTestObject('OR-RXFLEX-7/OR-Setup/OR-Formulary/OR-Page-Create-New-Formulary/preventative-list-yes'))

WebUI.scrollToElement(findTestObject('OR-RXFLEX-7/OR-Setup/OR-Formulary/OR-Page-Create-New-Formulary/tiers-name-arrow'), 
    0)

'/*Use: To select "Yes" for UM list */'
WebUI.verifyElementVisible(findTestObject('OR-RXFLEX-7/OR-Setup/OR-Formulary/OR-Page-Create-New-Formulary/um-list-yes'))

WebUI.click(findTestObject('OR-RXFLEX-7/OR-Setup/OR-Formulary/OR-Page-Create-New-Formulary/um-list-yes'))

'/*Use: To select Edit Types from EDIT TYPES list  */'
WebUI.click(findTestObject('OR-RXFLEX-7/OR-Setup/OR-Formulary/OR-Page-Create-New-Formulary/select-um-list-value1'))

WebUI.click(findTestObject('OR-RXFLEX-7/OR-Setup/OR-Formulary/OR-Page-Create-New-Formulary/select-um-list-value2'))

'/*Use: To enter Ticket Description */'
WebUI.scrollToElement(findTestObject('OR-RXFLEX-7/OR-Setup/OR-Formulary/OR-Page-Create-New-Formulary/ticket-description'), 
    0)

WebUI.setText(findTestObject('OR-RXFLEX-7/OR-Setup/OR-Formulary/OR-Page-Create-New-Formulary/ticket-description'), 'Testing ticket description please ignore. Team Adaptive.')

'/*Use: To select Create Ticket */'
WebUI.click(findTestObject('OR-RXFLEX-7/OR-Setup/OR-Formulary/OR-Page-Create-New-Formulary/btn-create-ticket'))

//WebUI.verifyTextPresent('', false)

